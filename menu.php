<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="index.php">APMAR</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="organization.php">Organization</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="submission.php">Submission</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="demo.php">Demo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="venue.php">Venue</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Registration</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sponsorship.php">Sponsorship</a>
          </li>
        </ul>
      </div>
</nav>