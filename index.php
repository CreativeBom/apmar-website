<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">
          <h2>About</h2>
          <p>The 2nd Asia Pacific Workshop on Mixed and Augmented Reality (APMAR) will be held in Taipei on April 13-15, 2018.  APMAR is the successor of the Korea-Japan Workshop on Mixed Reality (KJMR) which has been held between 2008 and 2015, and the Asia-Pacific Workshop on Mixed Reality (APMR) held in Andong, Korea in 2016. The 1st APMAR has been successfully taken place at Beijing on July 2-4, 2017.</p>

      	  <p>The 2nd APMAR is now seeking for submissions related to AR, VR and MR. In the last decade Mixed Reality (MR) has evolved from being experimental technologies to mainstream research activities that are starting to show impact on industry and society worldwide. The aim of this workshop is to create opportunities for sharing and exchanging research results and practical experiences in this exciting field.</p>

          
          
        </div>
        <?php include("side.php"); ?>
        <div class="col-md-12">
          <br>
          <h2>Keynote Speakers</h2>
          <p>This year we are honored to have … </p>
        </div>
        <div class="col-md-4">
          <img src="img/MasahikoINAMI.png" alt="Prof. Masahiko INAMI" width="200">
          <p><a href="http://www.rcast.u-tokyo.ac.jp/research/people/staff-inami_masahiko_en.html">Prof. Masahiko Inami</a></p>
          </div>
        <div class="col-md-4">
          <img src="img/MingOuhyoung.png" alt="Prof. Ming Ouhyoung" height="247">
          <p><a href="https://www.csie.ntu.edu.tw/~ming/index.html">Prof. Ming Ouhyoung</a></p>
        </div>

        <div class="col-md-8">
          <br>
          <h2>Supported by</h2>
          <h6>Taiwan Association of Computer-Human Interaction</h6>
          <h6>ACM SIGCHI Taipei Chapter</h6>
          
          <br>
          <h2>Platinum Level Sponsors:</h2>
          <img src="img/NTU_LOGO.png" alt="台大" width="100" style="padding-left:20px">
          <img src="img/caoslogo.png" alt="中研院" width="100" style="padding-left:20px">
          <img src="img/imgRead.jpg" alt="南藝大" height="95" style="padding-left:20px; padding-top:10px">
          <img src="img/IoX Center Logo_preview.png" alt="NTU IoX Center" height="100" style="padding-left:10px">
          
          <br><br>
          <h2>Gold Level Sponsors:</h2>
          <br>
          <a href="http://taiwanchi.org/apmar2018/sponsorship.php">Sponsorship information</a>
          <br>
          <br>
          <br>

        </div>
        

      </div>

    <?php include("footer.php"); ?>
  </body>
</html>