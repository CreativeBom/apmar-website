<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>Call for Demos</h2>
          <p>APMAR2018 seeks hands-on demonstrations sharing novel and innovative prototypes, devices, interaction techniques or systems from any area of virtual reality, augmented reality or Mixed reality.<br>
          The best demo award recipient will receive a $1,000 USD grant.<br>
          APMAR demos are non-archival and APMAR allows resubmission of demos previously shown at other venues. We encourage submissions from research, industry, startups, maker communities, the arts, and design communities.<br>
          Authors of all submitted papers will also be invited and encouraged to participate in the demo session.
          </p>

          <h4>Submission Information:</h4>
          <p>The deadline for demo submissions is January 29, 2018 (23:59 PST).<br>
          Use the CMT3 Submission System to submit your work: <a href="https://cmt3.research.microsoft.com/APMAR2018Demo/">https://cmt3.research.microsoft.com/APMAR2018Demo/</a></p>

          <h4>Demo Format:</h4>
          <p>Two pages, including a contact info, a description and a figure showing an overview of the demo; as well as technical requirements. 
          The conference will provide basic infrastructure, like a table and power, and space either with daylight or in a dark room. <br>
          <p>APMAR DEMO template: <a href="http://taiwanchi.org/apmar2018/apmar_demo.docx">{download}</a></p>
          <p>We highly encourage authors to submit a short video (no longer than 5 minutes) showing their demo, in MP4 format using the H.264 codec.</p>
          
          <h4>Demonstrations Selection Process:</h4>
          <p>The selection process includes reviews by the Demonstrations program committee, and will take into account feasibility, available space at the conference, and other relevant information. Submissions should NOT be anonymous. However, confidentiality of submissions will be maintained during the selection process. All rejected submissions will be kept confidential in perpetuity.</p>

          <h4>Attendance:</h4>
          <p>Accepted demos must be set up and running during demo sessions at the conference. Demo acceptance is always conditional on at least one author registering for the conference and presenting the work in person.</p>

          <h4>Awards:</h4>
          <p>Demos are an important part of APMAR2018. They provide researchers with an opportunity to present their latest work that would provoke discussion. Thus, we've decided to provide awards for the best demo (Attendee votes). The best demo award recipient will receive a $1,000 USD grant.</p>

          

        </div>
        <?php include("side.php"); ?>
      </div>

    <?php include("footer.php"); ?>
  </body>
</html>