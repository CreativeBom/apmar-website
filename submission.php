<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>Submission</h2>
          <h4>Format:</h4>
          <p>All papers must be formatted according to the ACM SIGCHI Paper and Notes format. Click on the link to access the template for this format.  (<a href="https://sigchi.org/templates/">https://sigchi.org/templates/</a>)</p>
          <h4>Paper Length: </h4>
          <p>2-4 pages, including all text, figures, and references.</p>
          <h4>Submission Site: </h4>
          <p>Submissions should be made through the cmt3 conference website, <a href="https://cmt3.research.microsoft.com/APMAR2018">https://cmt3.research.microsoft.com/APMAR2018</a></p>

          <h4>Review Process: </h4>
          <p>Single blind review. Authors should put their names and affiliations on the paper submitted. The names of the reviewers are hidden from the authors.</p>

        </div>
        <?php include("side.php"); ?>
      </div>

    <?php include("footer.php"); ?>
  </body>
</html>