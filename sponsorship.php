<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>Sponsorship Information</h2>
          

			<p>APMAR 2018 welcomes corporate and philanthropic sponsors to help support this event which brings together students, researchers, and employees of relevant industries. Sponsors will receive benefits depending on the sponsorship levels. We have two levels of corporate sponsorship available to you:</p>

			<h4>Platinum Level ($2,000 USD & above)</h4>
			<ul>
				<li>Three complimentary conference registrations</li>
				<li>One complimentary exhibition booth at the conference venue</li>
				<li>Full page advertisement in conference program</li>
				<li>Logo on the APMAR2018 website (under Platinum Level)</li>
			</ul>

			<h4>Gold Level ($1,000 USD)</h4>
			<ul>
				<li>One complimentary conference registrations</li>
				<li>One complimentary exhibition booth at the conference venue</li>
				<li>1/2 page advertisement in conference program</li>
				<li>Logo on the APMAR2018 website (under Gold Level)</li>
			</ul>

        </div>
        <?php include("side.php"); ?>
      </div>

    <?php include("footer.php"); ?>
  </body>
</html>