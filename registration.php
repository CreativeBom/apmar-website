<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>Registration Form</h2>
          

			<p>Please fill out the form below. Note that you will be asked to pay immediately upon submitting this form: payment can only be made with credit/bank card (VISA, MasterCard, JCB, and UnionPay).</p>

			<p>Each accepted paper needs at least “one full registration” before the camera-ready deadline on Feb. 28.</p>

			<br><h4>Registration Package Includes</h4>
			<ul>
				<li>Access to all sessions at APMAR 2018</li>
				<li>One banquets ticket (for full and student registrations)</li>
			</ul>

			<br><h4>Registration Dates and Fees</h4>
			<ul>
				<li>Early registration: on or before Feb. 28</li>
				<li>Standard registration: Mar. 1 – Apr. 10</li>
				<li>On-site registration: Apr. 13 – Apr. 15</li>
			</ul>

			<p>All charges are made in New Taiwan dollars (TWD).</p>

			<table class="table">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">early registration<br>(until Feb. 28)</th>
			      <th scope="col">standard registration<br>(Mar. 1 – Apr. 10)</th>
			      <th scope="col">on-site registration<br>(Apr. 13 – Apr. 15)</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">full registration</th>
			      <td>10,000 TWD<br>(~333 USD)</td>
			      <td>12,000 TWD<br>(~400 USD)</td>
			      <td>13,000 TWD<br>(~433 USD)</td>
			    </tr>
			    <tr>
			      <th scope="row">student registration</th>
			      <td>5,000 TWD<br>(~167 USD)</td>
			      <td>7,000 TWD<br>(~233 USD)</td>
			      <td>8,000 TWD<br>(~267 USD)</td>
			    </tr>
			  </tbody>
			</table>

			<p>Student registration rates are available only to undergraduate and graduate students. For those making student registrations, your student ID cards or the proof of your student status may be checked when getting your badges at the workshop.</p>

			<br><h4>Cancellation Policy</h4>
			<p>All refund requests must be made via email (registration@apmar2018.org). All cancellations will be subject to a cancellation fee of 3000 TWD (~100 USD) before Mar. 13, 2018. NO REFUNDS will be made after Mar. 13, 2018.</p>

			<br><h4>Visa-related Questions</h4>
			<p>Invitation letter can be requested during the registration process. More information about visa can be found at <a href="http://taiwanchi.org/apmar2018/visa/" target="_blank">http://taiwanchi.org/apmar2018/visa/</a>.For authors and participants from China, we suggest that you could start your visa applications as soon as possible. Please refer to this document.</p>

			<br><h4>Questions</h4>
			<p>All questions regarding registration and payment can be addressed to (registration@apmar2018.org)</p>

			<br><div class="container col-md-4 col-md-offset-4">
			<button type="button" class="btn btn-info">APMAR Registration Website</button><br><br><br>
			</div>

        </div>
        <?php include("side.php"); ?>
      </div>

    <?php include("footer.php"); ?>
  </body>
</html>