<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>Workshop Venue</h2>
          <h4>Location:</h4>
          <p>APMAR 2018 will be held in Auditorium 101, Barry Lam Hall, National Taiwan University, Taipei, Taiwan.</p>
          
          <img src="img/BarryLamHall.jpg" alt="Barry Lam Hall">
          <p>Barry Lam Hall</p>
          <img src="img/Auditorium101.jpg" alt="Auditorium 101" width="512">
          <p>Auditorium 101</p>

        </div>

        <?php include("side.php"); ?>

        <div class="col-md-8">
        <h4>About Taiwan:</h4>
        <p>Taipei, officially known as Taipei City, is the capital of Taiwan, ROC. Situated at the northern tip of Taiwan, ROC, Taipei is located on the Tamsui River; it is about 25 km (16 mi) southwest of Keelung, a port city on the Pacific Ocean. It lies in the Taipei Basin, an ancient lakebed bounded by the two relatively narrow valleys of the Keelung and Xindian rivers, which join to form the Tamsui River along the city’s western border. The city proper is home to an estimated 2,618,772 people. Taipei, New Taipei, and Keelung together form the Taipei–Keelung metropolitan area with a population of 6,900,273. They are administered under three municipal governing bodies. “Taipei" sometimes refers to the whole metropolitan area, while “Taipei City" refers to the city proper. Taipei City proper is surrounded on all sides by New Taipei.</p>
        </div>

        <div class="col-md-8">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/_UFiKXjEInk?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
        
        <div class="col-md-8">
        <br>
        <h4>Local Attractions in Taipei:</h4>
          <div class="row">
          <div class="col-xs-4 col-sm-4">
            <a href=" http://www.accv2016.org/uncategorized/xinbeitou-hot-spring-area/" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/xin2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Xinbeitou Hot Spring Area</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="http://www.accv2016.org/uncategorized/national-palace-museum/" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/npm2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>National Palace Museum</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/426" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/ddw2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Dadaocheng Wharf</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/446" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/nsy2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>National Sun Yat-sen Memorial Hall</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/1692" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/shi2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Shilin Night Market</b></p>
            </a>
          </div>

          <!--div class="col-xs-4 col-sm-4">
            <a href="http://www.accv2016.org/uncategorized/tamsui-fishermans-wharf/" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/fish2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Tamsui Fisherman’s Wharf</b></p>
            </a>
          </div-->

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/537" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/zoo2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Taipei Zoo</b></p>
            </a>
          </div>

          <!--div class="col-xs-4 col-sm-4">
            <a href="http://www.accv2016.org/uncategorized/longshan-temple/" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/lon2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Longshan Temple</b></p>
            </a>
          </div-->

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/435" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/y2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Yangmingshan Hot Spring Area</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/540" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/1012.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Taipei 101 Mall</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/445" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/nck2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>National Chiang Kai-shek Memorial Hall</b></p>
            </a>
          </div>

          <div class="col-xs-4 col-sm-4">
            <a href="https://www.travel.taipei/en/attraction/details/642" target="_blank">
            <img width="215" height="160" src="http://www.accv2016.org/wp-content/uploads/2016/01/jiu2.jpg" class="vc_single_image-img attachment-full" alt="xin2">
            <p style="text-align: center;"><b>Jiufen</b></p>
            </a>
          </div>

        </div>

        <a href="https://www.travel.taipei/en/attraction" target="_blank">
          <p style="text-align: center;">Information source: taipeitravel.net</p>
        </a>
        
        </div>


      </div>

    <?php include("footer.php"); ?>
  </body>
</html>