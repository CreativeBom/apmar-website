<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      <div class="row">
        <div class="col-md-8">

          <h2>Organization</h2>
          <h4>Steering Committee</h4>
          <p>Mark Billinghurst | University of South Australia</p>
          <p>Robin Chen | National Taiwan University</p>
          <p>Henry Duh | La Trobe University</p>
          <p>Kiyoshi Kiyokawa | Nara Institute of Science and Technology</p>
          <p>Jongil Park | Hanyang University</p>
          <p>Hideo Saito | Keio University</p>
          <p>Yongtian Wang | Beijing Institute of Technology</p> 

          <h4>General Chairs</h4>
          <p>Yi-Ping Hung | National Taiwan University</p>
          <p>Hirokazu Kato | Nara Institute of Science and Technology</p>

          <h4>Program Chairs</h4>
          <p>Chu-Song Chen | Academia Sinica</p>
          <p>Yue Liu | Beijing Institute of Technology</p>
          <p>Nobuchika Sakata | Osaka University</p>

          <h4>Publicity Chairs</h4>
          <p>Youngho Lee | Mokpo National University</p>
          <p>Ju-Chun Ko | National Taipei University of Technology</p>

          <h4>Local Arrangement Chairs</h4>
          <p>Sheng-Wen Shih | National Chi Nan University</p>
          <p>Yu-Chiang Wang | National Taiwan University</p>

          <h4>Demo Chairs</h4>
          <p>Neng-Hao Yu | National Chengchi University</p>

          <h4>Publication Chair</h4>
          <p>Huei-Yung Lin | National Chung Cheng University</p>

          <h4>Web Chair</h4>
          <p>Ping-Hsuan Han | National Taipei University of Technology</p>

          <h4>Financial Chair</h4>
          <p>Kuan-Wen Chen | National Chiao Tung University</p>

          <h4>Registration Chair</h4>
          <p>Yen-Yu Lin | Academia Sinica</p>

          <h4>Industry Chair</h4>
          <p>Da-Yuan Huang | National Taiwan University of Science and Technology</p>

          <h4>Program Committee Member</h4>
          <p>Liwei Chan | National Chiao Tung University</p>
          <p>Bing-Yu Chen | National Taiwan University</p>
          <p>James Hung-Kuo Chu | National Tsing Hua University</p>
         <p>Yuichiroo Fujimoto | Tokyo University of Agriculture and Technology</p>
          <p>Min-Chun Hu | National Cheng Kung University</p>
          <p>Sei Ikeda | Ritsumeikan University</p>
         <p>Daisuke Iwai | Osaka University</p>
         <p>Masayuki Kanbara | Nara Institute of Science and Technology</p>
         <p>Itaru Kitahara | University of Tsukuba</p>
          <p>Gun Lee | University of South Australia</p>
          <p>Xiaohui Liang | Beihang University</p>
          <p>Bin-Da Liu | National Cheng Kung University</p>
          <p>Shohei Mori | Keio University</p>
         <p>Takuji Narumi | Tokyo University</p>
		 <p>Alexander Plopski | Naist</p>

          <p>Nong Sang | Huazhong University of Science and Technology</p>
         <p>Fumihisa Shibata | Ritsumeikan University</p>
          <p>Guo-Dung Su | National Taiwan University</p>
         <p>Maki Sugimoto | Keio University</p>
          <p>Wen-Kai Tai | National Taiwan University of Science and Technology</p>
          <p>Takafumi Taketomi | NAIST</p>
         <p>Yuki Uranishi | Osaka University</p>
          <p>Cheng Wang | Xiamen University</p>
         <p>Goshiro Yamamoto | Kyoto University</p>
          <p>Jar-Ferr Yang | National Cheng Kung University</p>
		  <p>Fengjun Zhang | Institute of Software, CAS</p>

        </div>
        <?php include("side.php"); ?>
      </div>
    <?php include("footer.php"); ?>
  </body>
</html>