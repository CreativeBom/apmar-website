<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>大陸地區專業人士申請來台作業流程說明</h2>
          <p>
            大陸地區專業人士參與亞太地區混合與擴增實境研討會的相關來台作業流程，請參見以下說明完成來台申請相關手續。
          </p>

          <p>
            完成線上報名註冊Asia Pacific Workshop on Mixed and Augmented Reality, 2018，並繳交入台證申請費，
            <STRONG>每人新台幣2000元整(約RMB400元)(此筆費用不予退費)</STRONG>。(線上報名系統)
          </p>



          <p><STRONG>注意事項</STRONG></p>
          <ol>
            <li>申請入台所有表格，請依照臺灣移民署規定，請勿變更表格內容，以免造成無法申辦情形。</li>
            <li>辦理入台證需花費一些時間進行初步資料查閱及申請相關文件時間，若已將入台證材料提交移民署，進入程序審查階段，而
              <strong>臨時要取消辦理入台證者或已經核發入台證要撤銷者，無法退費</strong>。</li>
            <li>預計來台之與會者眷屬，由於申請資料不同，煩請自行於境內旅行社辦理旅行簽證，造成不便之處，敬請見諒。</li>
            <li>除臺灣地區申請手續外，大陸地區入台申請手續亦約需一至二個月時程，若逾時申請，請自行洽詢台灣當地之旅行社以觀光名義辦理旅簽。</li>
            <li><STRONG>原訂入出境台灣行程若有變動，請務必提前告知大會承辦人員，以便向移民署進行報備作業。</STRONG>。</li>
          </ol>

          <p><STRONG>入出境許可證連絡窗口：</STRONG></p>
          <p>臺灣聯絡人:林芳而小姐</p>
          <p>e-mail:<a href="mailto:registration@apmar2018.org">registration@apmar2018.org</a></p>
          



      

        </div>
        <?php include("side.php"); ?>
      </div>

    <?php include("footer.php"); ?>
  </body>
</html>