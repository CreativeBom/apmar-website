<!DOCTYPE html>
<html lang="en">
  <?php include("header.php"); ?>
  <body>
    <?php include("menu.php"); ?>
    <?php include("banner.php"); ?>
    <div class="container">
      
      <div class="row">
        <div class="col-md-8">

          <h2>Visa</h2>
          <h4>Visa-Exempt Entry:</h4>
          <p>
            Nationals of the following countries are eligible for the visa exemption program, with a duration of stay of up to 90 days: Andorra, Australia (effective from January 1, 2015 to December 31, 2018), Austria, Belgium, Bulgaria, Canada, Chile, Croatia, Cyprus, Czech Republic, Denmark, El Salvador, Estonia, Finland, France, Germany, Greece, Haiti, Honduras*, Hungary, Iceland, Ireland, Israel, Italy, Japan*, Republic of Korea, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Monaco, the Netherlands, New Zealand, Nicaragua, Norway, Poland, Paraguay, Portugal, Romania, San Marino, Slovakia, Slovenia, Spain, Sweden, Switzerland, the United Kingdom, the United States of America*, and Vatican City State.
          </p>
          <p>
            Nationals of the following countries are eligible for the visa exemption program, with a duration of stay of up to 30 days: Belize*, Dominican Republic, Guatemala, Malaysia, Saint Christopher and Nevis*, Saint Lucia*, Saint Vincent and the Grenadines, Singapore.
          </p>
           <p>
           (Please click <a href="https://www.boca.gov.tw/cp-149-270-9fd80-2.html">here</a> for more visa-exempt information)
          </p>

          <h4>Visitor Visa for Attending Conference:</h4>
          <p>Participants who are not eligible for visa-empty entry MUST apply for visitor visa in advance. For details of the visitor visa application, please visit the Bureau of Consular Affairs’ (BOCA) website at <a href="https://www.boca.gov.tw/mp-2.html">https://www.boca.gov.tw/mp-2.html</a></p>

          <h4>Visa for Mainland China Passport Holders:</h4>
          <h5>(大陸地區專業人士申請來台作業流程說明)</h5>
          <p>Please click here  <a href="http://taiwanchi.org/apmar2018/大陸地區專業人士申請來台作業流程說明/">請點這</a> for further information.</p>


        </div>
        <?php include("side.php"); ?>
      </div>

    <?php include("footer.php"); ?>
  </body>
</html>